<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function CreatTable()
    {
        DB::table('product')->insert([
            ['id'=> '1', 'user_id' => '1', 'name'=>'Мультиварка', 'category'=>'Кухоная техника', 'number'=>'1', 'amount' => '600', 'created_at'=>0,'updated_at' => 0],
            ['id'=> '2', 'user_id' => '2', 'name'=>'Гитара', 'category'=>'Музыкальный центр', 'number'=>'5', 'amount' => '1600', 'created_at'=>0,'updated_at' => 0],
            ['id'=> '3', 'user_id' => '1', 'name'=>'Блендер', 'category'=>'Кухоная техника', 'number'=>'2', 'amount' => '600', 'created_at'=>0,'updated_at' => 0],
            ['id'=> '4', 'user_id' => '3', 'name'=>'Коврик', 'category'=>'Текстиль', 'number'=>'2', 'amount' => '120', 'created_at'=>0,'updated_at' => 0]
    }
    DB::table('users')
        ->join('product', function ($join) {
            $join->on('users.id', '=', 'product.user_id')
                 ->where('id', '<', 5);
        })
        ->get();
}
