<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function CreatTable()
    {
        DB::table('users')->insert([
            ['id'=>'1','name'=> 'Ivan', 'email' => 'ivan@example.com', 'email_verified_at'=>'20.11.21', 'password'=>'ivan85', 'remember_token'=>'1', 'created_a' => 0, 'updated_at' => 0],
            ['id'=>'2','name'=> 'Alex', 'email' => 'alex@example.com', 'email_verified_at'=>'21.11.21', 'password'=>'star85', 'remember_token'=>'2', 'created_a' => 0, 'updated_at' => 0],
            ['id'=>'3','name'=> 'Igor', 'email' => 'igor@example.com', 'email_verified_at'=>'22.11.21', 'password'=>'turn85', 'remember_token'=>'3', 'created_a' => 0, 'updated_at' => 0],
            ['id'=>'4','name'=> 'Max', 'email' => 'max@example.com', 'email_verified_at'=>'23.11.21', 'password'=>'ivan5685', 'remember_token'=>'4', 'created_a' => 0, 'updated_at' => 0],
            ['id'=>'5','name'=> 'Vlad', 'email' => 'vlad@example.com', 'email_verified_at'=>'20.11.21', 'password'=>'iva56n85', 'remember_token'=>'5', 'created_a' => 0, 'updated_at' => 0],
    }
   
    public function OneName()
    {
        $users = DB::table('users')
            ->where('name', '=', 'Ivan')
            ->where ('id', '>','2')
            ->get();
        dd($users);
    }
    public function Last()
    {
        $user = DB::table('users')
                ->latest()
                ->first();
        dd($user);
    }
}
