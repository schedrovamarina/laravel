<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::prefix('user')->name('users.')->group(function (){
   Route::get('/', [UserController::class, 'index'])->name ('list');
    Route::get('/{id}', [UserController::class, 'GetUser'])->name ('getOne');
    Route::post('/user', [UserController::class, 'create'])->name ('new');
    Route::put('/user', [UserController::class, 'edit'])->name ('change');;
    Route::get('/delete/{id}', [UserController::class, 'delete'])->name ('drop');; 
});


